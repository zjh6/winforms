# Windows Forms Designer Release Notes

The .NET Core Windows Forms visual designer will be part of a future Visual Studio 2019 update, but it is currently available as a pre-release Visual Studio extension.

Releases happen once per month and are available for download below. We strongly recommend using the latest stable release, but the older ones are available just in case.

## Latest Stable Release

- Download link to the [Installation Package (VSIX)](https://aka.ms/winforms-designer)
- Release notes: [releasenotes.md](0.1/releasenotes.md)
- Known issues: [knownissues.md](0.1/knownissues.md)
- Getting started: [gettingstarted.md](0.1/gettingstarted.md)
